import cv2
from random import randrange
path = 'image_example.jpg'
path2 = r'C:\Users\Akshit\AppData\Local\Programs\Python\Python37-32\Lib\site-packages\cv2\data\haarcascade_frontalface_default.xml'

trained_face_data = cv2.CascadeClassifier(
    path2)
# the zero defines that the picture is going to be black and white
img = cv2.imread(path)
# cv2.imshow('image', img)


face_cordintes = trained_face_data.detectMultiScale(img)
for (x, y, z, h) in face_cordintes:

    cv2.rectangle(img, (x, y), (x+z, y+h), (randrange(0, 256),
                                            randrange(0, 256), randrange(0, 256)), 4)


cv2.imshow('done', img)

# (x,y,w,h)=face_cordintes[0]
# cv2.rectangle(img,(x,y),(x+w,y+h),[0,255,0],2)

cv2.waitKey()
