import cv2
from matplotlib import pyplot as plt
path = r'C:\Users\Akshit\Downloads\megumin.png'
img = cv2.imread(path,0)
start_point = (0, 0)
  
# End coordinate, here (250, 250) 
# represents the bottom right corner of image 
end_point = (250, 250) 
  
# Green color in BGR 
color = (0, 255, 0) 
  
# Line thickness of 9 px 
thickness = 9
  
# Using cv2.line() method 
# Draw a diagonal green line with thickness of 9 px 
image = cv2.line(img, start_point, end_point, color, thickness)


cv2.imshow(r'C:\Users\Akshit\Downloads\megumin.png',image)
#this diplays the image using the imshow function

cv2.waitKey(0) 
  
# closing all open windows 
cv2.destroyAllWindows() 




